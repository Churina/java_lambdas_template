package io.catalyte.training;

import java.util.*;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.Arrays;
import java.util.Comparator;


public class LambdaExercise {

  /**
   * Takes a List of strings and displays each one to the console.
   *
   * @param words the List of Strings to be displayed.
   */
  public void displayList(List<String> words) {
//    for(int i = 0; i < words.size();i++){
//      System.out.println(words.get(i));
//    }

    words.forEach(word -> System.out.println(word));
  }

  /**
   * Accepts a List of Integers and displays the number in the console if it is an even number that
   * is below 100.
   *
   * @param numbers the List of Strings to be displayed.
   */
  public void displayIntegers(List<Integer> numbers) {
//      for(int i =0; i < numbers.size();i++){
//        if(numbers.get(i)%2 == 0 && numbers.get(i) < 100){
//          System.out.println(numbers.get(i));
//        }
//      }

//    for(int number: numbers){
//      if(number%2 == 0 && number < 100){
//        System.out.println(number);
//      }
//    }

    numbers.forEach(number -> {if(number%2 == 0 && number < 100) System.out.println(number);});
  }

  /**
   * Sums the given List of Integers
   *
   * @param numbers the List of Integers to be summed.
   * @return the sum as a primitive integer.
   */
  public int sumIntegers(List<Integer> numbers) {
//    int sum = 0;
//    for(int i =0;i < numbers.size();i++){
//        sum += numbers.get(i);
//    }
//    return sum;

    return numbers.stream().mapToInt(number -> number).sum();


//    final int[] sum = {0};
//    numbers.forEach( number -> sum[0] += number);
//    return sum[0];

//    AtomicInteger sum = new AtomicInteger();
//    numbers.forEach( number -> sum.addAndGet(number));
//    return sum.get();
}

  /**
   * Finds the average value for the given array of primitive integers.
   *
   * @param ints the array of primitive integers
   * @return the average given as a Double
   */
  public Double averageInts(int[] ints) {
//    double sum = 0;
//    double average =0;
//   for(int i = 0; i <ints.length;i++){
//     sum += ints[i];
//     average = sum /ints.length;
//   }
//    return average;

    return Arrays.stream(ints).mapToDouble(num -> num).average().getAsDouble();
  }

  /**
   * Filters a given list of usernames by removing any that match the given set of customer names.
   *
   * @param users     the List of users to be filtered.
   * @param customers the Set of customer names to be removed from the list of users.
   * @return the filtered list of users.
   */
  public List<String> filterList(Set<String> customers, List<String> users) {
//    ArrayList<String> filtered = new ArrayList<String>();
//    for(String user: users){
//        if (!customers.contains(user))
//          filtered.add(user);
//   }
//    return filtered;

    return users.stream().filter(user -> !customers.contains(user)).collect(Collectors.toList());
  }

  /**
   * Sorts an array of Strings so that words that contain the letter 'e' appear before all the other
   * words.
   *
   * @param words the array of strings to be sorted.
   * @return a sorted array of Strings.
   */
  public String[] sortByLetterE(String[] words) {
    //Arrays.sort(words, Comparator.comparingInt(a -> (a.contains("E") || a.contains("e") ? 0 : 1)));
    Arrays.sort(words,Comparator.comparingInt(a -> (a.toLowerCase().contains("e")?0 : 1)));
    return words;
  }

  /**
   * Takes an array of Strings and capitalizes the first letter of each word.
   *
   * @param words the array of strings to be capitalized.
   * @return a List of capitalized words.
   */
  public List<String> capitalizeAllWords(ArrayList<String> words) {

//    List<String> capitalizeWords = new ArrayList<String>();
//    for (String word : words) {
//      String s1 = word.substring(0, 1).toUpperCase();
//      String s2 = s1 + word.substring(1);
//      capitalizeWords.add(s2) ;
//    }
//    return capitalizeWords;

    List<String> capitalizeWords = new ArrayList<>();
    words.forEach(word -> capitalizeWords.add(word.substring(0,1).toUpperCase() + word.substring(1)));
    return capitalizeWords;


  }

  /**
   * Takes an array of Strings and filters it with the given Predicate.
   *
   * @param words     the List of words to be filtered.
   * @param predicate the predicate used to evaluate the given list of words
   * @return a filtered List
   */
    public List<String> filterWords(ArrayList<String> words, Predicate<String> predicate) {

      return words.stream().filter(predicate).collect(Collectors.toList());
  }

//  public Stream<String> filterWords(ArrayList<String> words, Predicate<String> predicate) {
//
//    Stream<String> result = words.stream().filter(word -> predicate.test(word));
//    return result;
//  }


  /**
   * Takes a list of temperatures as ints and returns the minimum and maximum values.
   *
   * @param t the List of temperatures to be summarized.
   * @return a HashMap containing the minimum and maximum temperatures.
   */
  public HashMap<String, Integer> summarizeWeatherData(List<Integer> t) {

    Integer maxNumber = t.stream().max(Comparator.comparing(Integer::valueOf)).get();
    Integer minNumber = t.stream().min(Comparator.comparing(Integer::valueOf)).get();

    HashMap<String,Integer> temperatures = new HashMap<>();
    temperatures.put("Maximum",maxNumber);
    temperatures.put("Minimum",minNumber);
    return temperatures;
  }
}
